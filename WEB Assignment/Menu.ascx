﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Menu.ascx.cs" Inherits="WEB_Assignment.Menu" %>
<!-- A grey navbar that expands horizontally at medium device --> 
<nav class="navbar navbar-expand-md bg-light navbar-light">
 <!-- The brand(or icon) of the navbar -->
 <a class="navbar-brand" href="Home.aspx"
   style="font-size:32px; font-weight:bold; color:#3399FF;">
   NP Student Portfolio
 </a>
 <!-- Toggle/collapsible Button, also known as hamburger button --> 
 <button class="navbar-toggler" type="button"
     data-toggle="collapse" data-target="#staffNavbar"> 
   <span class="navbar-toggler-icon"></span>
 </button>
     <!-- Links in the navbar, displayed as drop-down list when collapsed -->
 <div class="collapse navbar-collapse" id="staffNavbar">
  <!-- Links that are aligned to the left,
    pi-auto: right margin auto-adjusted -->
    <ul class="navbar-nav w-auto">
   <li class="nav-item dropdown" id="change" runat="server">
   </li>
   <li class="nav-item">
    <a class="nav-link" href="ViewPortfolio.aspx">View Portfolio</a>
   </li>
   <li class="nav-item">
    <a class="nav-link" href="https://www.np.edu.sg/">Ngee Ann Polytechnic</a>
   </li>
   <li class="nav-item">
    <a class="nav-link" href="ContactUs.aspx">
      Contact Us</a>
   </li>
   <li class="nav-item" id="logout" runat="server">
       <asp:Button ID='BTNLogout' runat='server' Text='Logout' OnClick='BTNLogout_Click' />
   </li>
  </ul>
 </div>
</nav>