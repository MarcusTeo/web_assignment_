﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteTemplate.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WEB_Assignment.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style2 {
        width: 200px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table class="w-100">
    <tr>
        <td class="auto-style2">
            <asp:Label ID="Label1" runat="server" Text="Email Address"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">
            <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">
            <asp:Label ID="Label3" runat="server" Text="Test Logins"></asp:Label>
        </td>
        <td>
            <asp:Button ID="BTNLogin" runat="server" OnClick="BTNLogin_Click" Text="Parent" />
            <asp:Button ID="BTNParent" runat="server" OnClick="BTNParent_Click" Text="Parent" />
            <asp:Button ID="BTNStudent" runat="server" OnClick="BTNStudent_Click" Text="Student" />
            <asp:Button ID="BTNMentor" runat="server" OnClick="BTNMentor_Click" Text="Mentor" />
            <asp:Button ID="BTNAdmin" runat="server" OnClick="BTNAdmin_Click" Text="Admin" />
        </td>
    </tr>
</table>

</asp:Content>
