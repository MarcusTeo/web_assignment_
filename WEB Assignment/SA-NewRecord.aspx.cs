﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_Assignment
{
    public partial class SA_NewRecord : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                ddlNationality.Items.Add("Singapore");
                ddlNationality.Items.Add("Malaysia");
                ddlNationality.Items.Add("Indonesia");
                ddlNationality.Items.Add("China");
                ddlNationality.Items.Add("India");
                ddlNationality.Items.Add("Korea");
                ddlNationality.Items.Add("Korea");
                ddlNationality.Items.Add("Japan");

                ddlCourses.Items.Add("ICT");
                ddlCourses.Items.Add("FI");

                txtMName.Visible = false;
                txtSName.Visible = false;
                lblMName.Visible = false;
                lblSName.Visible = false;
            
            }
        }

        protected void radStud_CheckedChanged(object sender, EventArgs e)
        {
            if (radStud.Checked)
            {
                txtMName.Visible = false;
                lblMName.Visible = false;
                lblPNo.Visible = false;
                txtPNo.Visible = false;
                txtSName.Visible = true;
                lblSName.Visible = true;
                lblCourse.Visible = true;
                ddlCourses.Visible = true;
            }
        }

        protected void radMentor_CheckedChanged(object sender, EventArgs e)
        {
            if (radMentor.Checked)
            {
                lblCourse.Visible = false;
                ddlCourses.Visible = false;
                txtSName.Visible = false;
                lblSName.Visible = false;
                txtMName.Visible = true;
                lblMName.Visible = true;
                lblPNo.Visible = true;
                txtPNo.Visible = true;

            }
        }

        //protected void btnSubmit_Click(object sender, EventArgs e)
        //{
        //    if (Page.IsValid)
        //    {
        //        Staff objStaff = new Staff();
        //        string strValues;

        //        //Retrieve the inputs
        //        objStaff.name = txtSName.Text;
        //        strValues = "name=" + txtSName.Text;
        //        if (radMale.Checked)
        //        {
        //            objStaff.gender = "M";
        //        }
        //        else
        //        {
        //            objStaff.gender = "F";
        //        }

        //        objStaff.dob = Convert.ToDateTime(txtDOB.Text);
        //        objStaff.salary = Convert.ToDouble(txtSalary.Text);
        //        objStaff.eMail = txtEmail.Text;
        //        objStaff.nationality = ddlNationality.SelectedValue;
        //        objStaff.isFullTime = chkFullTime.Checked;

        //        int id = objStaff.add();

        //        if (radMale.Checked == true)
        //            strValues += "&gender=Male";
        //        else
        //            strValues += "&gender=Female";
        //        strValues += "&dob=" + txtDOB.Text;
        //        strValues += "&salary=" + txtPNo.Text;
        //        strValues += "&email=" + txtEmail.Text;
        //        strValues += "&nationality=" + ddlNationality.Text;

        //        if (chkFullTime.Checked == true)
        //        {
        //            strValues += "&fulltime=yes";
        //        }
        //        else
        //            strValues += "&fulltime=no";

        //        Response.Redirect("ConfirmAddStaff.aspx?" + "name=" + txtSName.Text + "&dob=" + txtDOB.Text);
        //    }

        //}


    }
}