﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_Assignment
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BTNLogin_Click(object sender, EventArgs e)
        {

        }

        protected void BTNParent_Click(object sender, EventArgs e)
        {
            Session["UserType"] = "parent";
            Response.Redirect("Home.aspx");
        }

        protected void BTNStudent_Click(object sender, EventArgs e)
        {
            Session["UserType"] = "student";
            Response.Redirect("Home.aspx");
        }

        protected void BTNMentor_Click(object sender, EventArgs e)
        {
            Session["UserType"] = "mentor";
            Response.Redirect("Home.aspx");
        }

        protected void BTNAdmin_Click(object sender, EventArgs e)
        {
            Session["UserType"] = "admin";
            Response.Redirect("Home.aspx");
        }
    }
}