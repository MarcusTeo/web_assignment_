﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteTemplate.Master" AutoEventWireup="true" CodeBehind="SA-NewRecord.aspx.cs" Inherits="WEB_Assignment.SA_NewRecord" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 175px;
        }
        .auto-style3 {
            width: 175px;
            height: 32px;
        }
        .auto-style4 {
            height: 32px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblType" runat="server" Text="Type: "></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:RadioButton ID="radStud" runat="server" GroupName="Type" OnCheckedChanged="radStud_CheckedChanged" Text="Student" AutoPostBack="True" />
                <asp:RadioButton ID="radMentor" runat="server" OnCheckedChanged="radMentor_CheckedChanged" Text="Mentor" AutoPostBack="True" GroupName="Type" />
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblSName" runat="server" Text="Student Name: "></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtSName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvSName" runat="server" ControlToValidate="txtSName" Display="Dynamic" ErrorMessage="Please enter your name." ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblMName" runat="server" Text="Mentor Name: "></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtMName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvMName" runat="server" ControlToValidate="txtMName" Display="Dynamic" ErrorMessage="Please enter your name." ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">
                <asp:Label ID="lblGender" runat="server" Text="Gender: "></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:RadioButton ID="radMale" runat="server" Checked="True" GroupName="Gender" Text="Male" />
                <asp:RadioButton ID="radFemale" runat="server" GroupName="Gender" Text="Female" />&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblEmail" runat="server" Text="E-mail: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please enter a unique email." ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblPNo" runat="server" Text="Phone Number: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtPNo" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="ravPNo" runat="server" ControlToValidate="txtPNo" Display="Dynamic" ErrorMessage="Please enter a valid phone number." ForeColor="Red" MinimumValue="8" Type="Integer" MaximumValue="16"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblNationality" runat="server" Text="Nationality: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlNationality" runat="server">
                </asp:DropDownList>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblCourse" runat="server" Text="Course: "></asp:Label>
            </td>
            <td>
                <%--<asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />--%>
                <asp:DropDownList ID="ddlCourses" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="lblAMentor" runat="server" Text="Assigned Mentor: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlAMentor" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
