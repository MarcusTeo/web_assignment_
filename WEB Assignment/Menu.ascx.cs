﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_Assignment
{
    public partial class Menu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((string)Session["UserType"] == "parent")
            {
                change.InnerHtml = "<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Parent</a><div class='dropdown-menu' aria-labelledby='navbarDropdown'><a class='dropdown-item' href='P-ViewChild.aspx'>Child Portfolio</a><a class='dropdown-item' href='Messages.aspx'>Messages</a></div>";
            }
            else if ((string)Session["UserType"] == "mentor")
            {
                change.InnerHtml = "<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Mentor</a><div class='dropdown-menu' aria-labelledby='navbarDropdown'><a class='dropdown-item' href='M-Mentees.aspx'>Mentees</a><a class='dropdown-item' href='M-ChangePassword.aspx'>Password</a><a class='dropdown-item' href='Messages.aspx'>Messages</a></div>";
            }
            else if ((string)Session["UserType"] == "student")
            {
                change.InnerHtml = "<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Student</a><div class='dropdown-menu' aria-labelledby='navbarDropdown'><a class='dropdown-item' href='S-UpdateProfile.aspx'>Update Profile</a><a class='dropdown-item' href='S-ViewProject.aspx'>View Project Portfolios</a><a class='dropdown-item' href='S-CreateProject.aspx'>Create Project Portfolios</a><a class='dropdown-item' href='Messages.aspx'>Messages</a></div>";
            }
            else if ((string)Session["UserType"] == "admin")
            {
                change.InnerHtml = "<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>System Administrator</a><div class='dropdown-menu' aria-labelledby='navbarDropdown'><a class='dropdown-item' href='SA-NewRecord.aspx'>New Record</a><a class='dropdown-item' href='SA-StudentRecords.aspx'>Student Records</a><a class='dropdown-item' href='SA-Courses.aspx'>Courses</a><a class='dropdown-item' href='SA-ViewRequests.aspx'>Parents View Requests</a></div>";
            }
            else
            {
                change.InnerHtml = "<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Login</a><div class='dropdown-menu' aria-labelledby='navbarDropdown'><a class='dropdown-item' href='Login.aspx'>Login</a><a class='dropdown-item' href='SignUp.aspx'>Sign Up as Parent</a></div>";
                logout.InnerHtml = "";
            }
        }

        protected void BTNLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("Home.aspx");
        }
    }
}