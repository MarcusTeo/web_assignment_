﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEB_Assignment
{
    public class Staff
    {
        //Properties
        public int staffId { get; set; }
        public string name { get; set; }
        public string gender { get; set; }
        public DateTime dob { get; set; }
        public string nationality { get; set; }
        public string eMail { get; set; }
        public double salary { get; set; }
        public bool isFullTime { get; set; }
        public int branchNo { get; set; }
    }
}